/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.controller;

import com.tungthai.foodapp2.dao.TaiKhoanDAO;
import com.tungthai.foodapp2.entity.TaiKhoan;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author tungthai
 */
@Controller
public class DangNhapController {
    @RequestMapping("/KiemTraDangNhap")
    public @ResponseBody int dangNhap(HttpServletRequest request){
        String email = request.getParameter("email");
        String tenTaiKhoan = request.getParameter("ten");
        String facebookID = request.getParameter("facebookId");
        
        System.out.println("\n\n\n\n\n-------------------------------");
        System.out.println("KIEM TRA DANG NHAP");
        System.out.println("\n\n\n\n\n-------------------------------");
        
        int id;
        TaiKhoan t = new TaiKhoan(tenTaiKhoan, email, facebookID);
        if(email != null){
            if(new TaiKhoanDAO().timTaiKhoanBangEmail(email) != null){
                id = new TaiKhoanDAO().suaTaiKhoan(t);
                System.out.println("1: "+id);
            } else {
                id = new TaiKhoanDAO().themTaiKhoan(t);
                System.out.println("2: "+id);
            }
        } else {
            if(new TaiKhoanDAO().timTaiKhoanBangFacebookID(facebookID) != null){
                id = new TaiKhoanDAO().suaTaiKhoan(t);
                System.out.println("3: "+id);
            } else {
                id  = new TaiKhoanDAO().themTaiKhoan(t);
                System.out.println("4: "+id);
            }
        }
        
        System.out.println("KHONG TIM THAY");
        System.out.println("\n\n\n\n\n\n");
        
        return id;
    }
}
