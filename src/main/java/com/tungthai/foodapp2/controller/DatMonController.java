/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.controller;

import com.tungthai.foodapp2.dao.CuaHangDAO;
import com.tungthai.foodapp2.dao.TaiKhoanDAO;
import com.tungthai.foodapp2.entity.CuaHang;
import com.tungthai.foodapp2.entity.MonAnCuaHang;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author tungthai
 */
@Controller
public class DatMonController {
    
    @RequestMapping("/DatMon")
    public String chuyenTrangDatMon(HttpServletRequest request) {
        String latHome = request.getParameter("latHome");
        String lngHome = request.getParameter("lngHome");
        String maCuaHang = request.getParameter("maCuaHang");
        String maTaiKhoan = request.getParameter("maTaiKhoan");
        
        CuaHang ch = new CuaHangDAO().timCuaHang(Integer.parseInt(maCuaHang));
        
        request.setAttribute("cuaHang", ch);
        request.setAttribute("latCuaHang", ch.getToaDoLat());
        request.setAttribute("lngCuaHang", ch.getToaDoLng());
        request.setAttribute("maTaiKhoan", maTaiKhoan);
        
        List<MonAnCuaHang> danhSachMonAn = new CuaHangDAO().danhSachMonAnCuaHang(Integer.parseInt(maCuaHang));
        
        request.setAttribute("danhSachMonAn", danhSachMonAn);
        
        int diemChamCuaHang = new TaiKhoanDAO().diemChamCuaHang(Integer.parseInt(maCuaHang), Integer.parseInt(maTaiKhoan));
        
        request.setAttribute("diemChamCuaHang", diemChamCuaHang);
        
        return "dat_mon";
    }
}
