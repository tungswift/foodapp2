/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.controller;

import com.tungthai.foodapp2.dao.CuaHangDAO;
import com.tungthai.foodapp2.entity.CuaHang;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author tungthai
 */
@Controller
public class HomeController {

    @RequestMapping(value = {"/", "/ChuyenTrang", "/TimKiem", "/home", "/index"})
    public String showHomePage(
            @RequestParam(value = "timKiem", required = false) String timKiem,
            @RequestParam(value = "trangHienTai", required = false) String trangHienTai,
            Model model) {
        
        List<CuaHang> list = null; //Cua hang tren trang chu
        List<CuaHang> cuaHangMoi = new CuaHangDAO().cuaHangMoi(); //3 cửa hàng trên slide
        int tongSoCuaHang = 0;
        int tongSoTrang;

        if (trangHienTai == null) {
            trangHienTai = "1";
        }
        
        if (timKiem != null) {
            HashMap<Integer, List<CuaHang>> map = (HashMap<Integer, List<CuaHang>>) new CuaHangDAO().timKiemCuaHang(timKiem, Integer.parseInt(trangHienTai), 9);
            for (Map.Entry<Integer, List<CuaHang>> entry : map.entrySet()) {
                tongSoCuaHang = entry.getKey();
                list = entry.getValue();
            }

            model.addAttribute("timKiem", timKiem);
        } else {
            list = new CuaHangDAO().danhSachCuaHang(Integer.parseInt(trangHienTai), 9);
            
            tongSoCuaHang = new CuaHangDAO().tongSoCuaHang();

            model.addAttribute("timKiem", "false");
        }

        //Phân trang
        tongSoTrang = tongSoCuaHang / 9;

        if (tongSoTrang < 1) {
            tongSoTrang = 1;
        } else if (tongSoTrang % 9 != 0) {
            tongSoTrang += 1;
        }

        if (trangHienTai == null) {
            trangHienTai = "1";
        } else {
            if (Integer.parseInt(trangHienTai) <= 0) {
                trangHienTai = "1";
            }
            if (Integer.parseInt(trangHienTai) > tongSoTrang) {
                trangHienTai = tongSoTrang + "";
            }
        }

        model.addAttribute("trangHienTai", trangHienTai);
        model.addAttribute("tongSoTrang", tongSoTrang);
        model.addAttribute("danhSachCuaHang", list);
        model.addAttribute("cuaHangMoi", cuaHangMoi);

        return "index";
    }
    
    @RequestMapping("/DangNhap")
    public String dangNhap(){
        return "login";
    }
}
