/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.dao;

import com.tungthai.foodapp2.entity.CuaHang;
import com.tungthai.foodapp2.entity.DiemChamCuaHang;
import com.tungthai.foodapp2.entity.DiemChamCuaHangPK;
import com.tungthai.foodapp2.entity.MonAnCuaHang;
import com.tungthai.foodapp2.entity.TaiKhoan;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

/**
 *
 * @author tungthai
 */
public class TaiKhoanDAO {

    private SessionFactory sessionFactory = null;
    private Session session = null;

    public int themTaiKhoan(TaiKhoan t) {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            session.save(t);

            session.getTransaction().commit();
            session.close();
            return t.getMaTaiKhoan();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public int suaTaiKhoan(TaiKhoan t) {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            session.update(t);

            session.getTransaction().commit();
            session.close();
            return t.getMaTaiKhoan();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }

    public boolean taiKhoanMoi() {
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            Query query = session.createQuery("from TaiKhoan");
            query.setFirstResult(0);
            query.setMaxResults(10);
            session.getTransaction().commit();
            session.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public TaiKhoan timTaiKhoanBangEmail(String email){
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            Query query = session.createQuery("from TaiKhoan where email = :email");
            query.setParameter("email", email);
            
            TaiKhoan t = (TaiKhoan) query.uniqueResult();
            
            session.getTransaction().commit();
            session.close();
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public TaiKhoan timTaiKhoanBangFacebookID(String facebookID){
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            Query query = session.createQuery("from TaiKhoan where facebookID = :facebookID");
            query.setParameter("facebookID", facebookID);
            
            TaiKhoan t = (TaiKhoan) query.uniqueResult();

            session.close();
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public int diemChamCuaHang(int maCuaHang, int maTaiKhoan){
        sessionFactory = new Configuration()
                .configure()
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        
        try {
            Query query = session.createQuery("from DiemChamCuaHang where diemChamCuaHangPK.maCuaHang = :maCuaHang "
                    + "and diemChamCuaHang.maTaiKhoan = :maTaiKhoan");
            query.setParameter("maCuaHang", maCuaHang);
            query.setParameter("maTaiKhoan", maTaiKhoan);
            
            int diem = (int) query.uniqueResult();
            
            session.close();
            return diem;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}
