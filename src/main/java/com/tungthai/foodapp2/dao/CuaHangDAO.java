/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.dao;

import com.tungthai.foodapp2.entity.CuaHang;
import com.tungthai.foodapp2.entity.MonAnCuaHang;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;


/**
 *
 * @author tungthai
 */
public class CuaHangDAO {

    private SessionFactory sessionFactory = null;
    private Session session = null;

    public List<CuaHang> danhSachCuaHang(int trangHienTai, int phanTuTrongMotTrang) {
        List<CuaHang> list;
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            Query query = session.createQuery("from CuaHang");
            query.setFirstResult((trangHienTai - 1) * phanTuTrongMotTrang);
            query.setMaxResults(phanTuTrongMotTrang);
            list = query.getResultList();

            session.getTransaction().commit();
            session.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }

    public int tongSoCuaHang() {
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            Query query = session.createQuery("select count(MaCuaHang) from CuaHang");
            long count = (long) query.uniqueResult();
            session.getTransaction().commit();
            session.close();

            return (int) count;
        } catch (Exception e) {
            e.printStackTrace();

            return -1;
        } finally {
            session.close();
            sessionFactory.close();

        }
    }

    public List<CuaHang> cuaHangMoi() {
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        List<CuaHang> list;
        try {
            session.beginTransaction();

            Query query = session.createQuery("from CuaHang");
            query.setFirstResult(0);
            query.setMaxResults(3);
            list = query.getResultList();

            session.getTransaction().commit();
            session.close();
            return list;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public HashMap<Integer, List<CuaHang>> timKiemCuaHang(String timKiem, int trangHienTai, int phanTuTrongMotTrang){
        List<CuaHang> list;
        HashMap<Integer, List<CuaHang>> map = new HashMap<>();
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();

            Query query = session.createQuery("from CuaHang where tenCuaHang like :timKiem");
            query.setFirstResult((trangHienTai - 1) * phanTuTrongMotTrang);
            query.setMaxResults(phanTuTrongMotTrang);
            query.setParameter("timKiem", "%"+timKiem+"%");
            list = query.list();

            Query query2 = session.createQuery("select count(MaCuaHang) from CuaHang where tenCuaHang like :timKiem");
            query2.setParameter("timKiem", "%"+timKiem+"%");
            long count = (long) query2.uniqueResult();
            session.getTransaction().commit();
            session.close();
            
            map.put((int) count, list);
            
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
    
    public CuaHang timCuaHang(int maCuaHang) {
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("from CuaHang where maCuaHang = :maCuaHang");
            query.setParameter("maCuaHang", maCuaHang);
            
            CuaHang cuaHang = (CuaHang) query.uniqueResult();
            session.getTransaction().commit();
            session.close();

            return cuaHang;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        } finally {
            session.close();
            sessionFactory.close();

        }
    }
    
    public List<MonAnCuaHang> danhSachMonAnCuaHang(int maCuaHang) {
        List<MonAnCuaHang> list;
        sessionFactory = new Configuration()
                .configure()
                .addAnnotatedClass(CuaHang.class)
                .addAnnotatedClass(MonAnCuaHang.class)
                .buildSessionFactory();
        session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("from CuaHang where maCuaHang = :maCuaHang");
            query.setParameter("maCuaHang", maCuaHang);
            
            CuaHang cuaHang = (CuaHang) query.uniqueResult();
            System.out.println(cuaHang.getMonAnCuaHangCollection());
            list = cuaHang.getMonAnCuaHangCollection();
            session.getTransaction().commit();
            session.close();
            
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}
