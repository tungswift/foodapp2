/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tungthai
 */
@Entity
@Table(name = "CuaHang")
public class CuaHang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MaCuaHang")
    private Integer maCuaHang;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TenCuaHang")
    private String tenCuaHang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "DiaChi")
    private String diaChi;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "LoaiCuaHang")
    private String loaiCuaHang;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "Logo")
    private String logo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GioMoCua")
    private int gioMoCua;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GioDongCua")
    private int gioDongCua;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TongDiem")
    private int tongDiem;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SoLuotCham")
    private int soLuotCham;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "ToaDoLat")
    private Double toaDoLat;
    @Column(name = "ToaDoLng")
    private Double toaDoLng;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "maCuaHang")
    private List<MonAnCuaHang> monAnCuaHangCollection;

    public CuaHang() {
    }

    public CuaHang(Integer maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    public CuaHang(Integer maCuaHang, String tenCuaHang, String diaChi, String loaiCuaHang, String logo, int gioMoCua, int gioDongCua, int tongDiem, int soLuotCham) {
        this.maCuaHang = maCuaHang;
        this.tenCuaHang = tenCuaHang;
        this.diaChi = diaChi;
        this.loaiCuaHang = loaiCuaHang;
        this.logo = logo;
        this.gioMoCua = gioMoCua;
        this.gioDongCua = gioDongCua;
        this.tongDiem = tongDiem;
        this.soLuotCham = soLuotCham;
    }

    public Integer getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(Integer maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    public String getTenCuaHang() {
        return tenCuaHang;
    }

    public void setTenCuaHang(String tenCuaHang) {
        this.tenCuaHang = tenCuaHang;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getLoaiCuaHang() {
        return loaiCuaHang;
    }

    public void setLoaiCuaHang(String loaiCuaHang) {
        this.loaiCuaHang = loaiCuaHang;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getGioMoCua() {
        return gioMoCua;
    }

    public void setGioMoCua(int gioMoCua) {
        this.gioMoCua = gioMoCua;
    }

    public int getGioDongCua() {
        return gioDongCua;
    }

    public void setGioDongCua(int gioDongCua) {
        this.gioDongCua = gioDongCua;
    }

    public int getTongDiem() {
        return tongDiem;
    }

    public void setTongDiem(int tongDiem) {
        this.tongDiem = tongDiem;
    }

    public int getSoLuotCham() {
        return soLuotCham;
    }

    public void setSoLuotCham(int soLuotCham) {
        this.soLuotCham = soLuotCham;
    }

    public Double getToaDoLat() {
        return toaDoLat;
    }

    public void setToaDoLat(Double toaDoLat) {
        this.toaDoLat = toaDoLat;
    }

    public Double getToaDoLng() {
        return toaDoLng;
    }

    public void setToaDoLng(Double toaDoLng) {
        this.toaDoLng = toaDoLng;
    }

    @XmlTransient
    public List<MonAnCuaHang> getMonAnCuaHangCollection() {
        return monAnCuaHangCollection;
    }

    public void setMonAnCuaHangCollection(List<MonAnCuaHang> monAnCuaHangCollection) {
        this.monAnCuaHangCollection = monAnCuaHangCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maCuaHang != null ? maCuaHang.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CuaHang)) {
            return false;
        }
        CuaHang other = (CuaHang) object;
        if ((this.maCuaHang == null && other.maCuaHang != null) || (this.maCuaHang != null && !this.maCuaHang.equals(other.maCuaHang))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tungthai.foodapp2.entity.CuaHang[ maCuaHang=" + maCuaHang + " ]";
    }
    
}
