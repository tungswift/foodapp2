/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tungthai
 */
@Entity
@Table(name = "TaiKhoan")
public class TaiKhoan implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "taiKhoan")
    private Collection<DiemChamCuaHang> diemChamCuaHangCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MaTaiKhoan")
    private Integer maTaiKhoan;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "TenTaiKhoan")
    private String tenTaiKhoan;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 300)
    @Column(name = "Email")
    private String email;
    @Size(max = 300)
    @Column(name = "FacebookID")
    private String facebookID;

    public TaiKhoan() {
    }

    public TaiKhoan(Integer maTaiKhoan) {
        this.maTaiKhoan = maTaiKhoan;
    }

    public TaiKhoan(String tenTaiKhoan, String email, String facebookID) {
        this.tenTaiKhoan = tenTaiKhoan;
        this.email = email;
        this.facebookID = facebookID;
    }
    

    public Integer getMaTaiKhoan() {
        return maTaiKhoan;
    }

    public void setMaTaiKhoan(Integer maTaiKhoan) {
        this.maTaiKhoan = maTaiKhoan;
    }

    public String getTenTaiKhoan() {
        return tenTaiKhoan;
    }

    public void setTenTaiKhoan(String tenTaiKhoan) {
        this.tenTaiKhoan = tenTaiKhoan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookID() {
        return facebookID;
    }

    public void setFacebookID(String facebookID) {
        this.facebookID = facebookID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maTaiKhoan != null ? maTaiKhoan.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TaiKhoan)) {
            return false;
        }
        TaiKhoan other = (TaiKhoan) object;
        if ((this.maTaiKhoan == null && other.maTaiKhoan != null) || (this.maTaiKhoan != null && !this.maTaiKhoan.equals(other.maTaiKhoan))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tungthai.foodapp2.entity.TaiKhoan[ maTaiKhoan=" + maTaiKhoan + " ]";
    }

    @XmlTransient
    public Collection<DiemChamCuaHang> getDiemChamCuaHangCollection() {
        return diemChamCuaHangCollection;
    }

    public void setDiemChamCuaHangCollection(Collection<DiemChamCuaHang> diemChamCuaHangCollection) {
        this.diemChamCuaHangCollection = diemChamCuaHangCollection;
    }
    
}
