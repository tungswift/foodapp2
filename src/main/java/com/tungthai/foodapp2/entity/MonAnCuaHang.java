/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author tungthai
 */
@Entity
@Table(name = "MonAnCuaHang")
public class MonAnCuaHang implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MaMonAn")
    private Integer maMonAn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "TenMonAn")
    private String tenMonAn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DonGia")
    private int donGia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SoLanDat")
    private int soLanDat;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "AnhMinhHoa")
    private String anhMinhHoa;
    @JoinColumn(name = "MaCuaHang", referencedColumnName = "MaCuaHang")
    @ManyToOne(optional = false)
    private CuaHang maCuaHang;

    public MonAnCuaHang() {
    }

    public MonAnCuaHang(Integer maMonAn) {
        this.maMonAn = maMonAn;
    }

    public MonAnCuaHang(Integer maMonAn, String tenMonAn, int donGia, int soLanDat, String anhMinhHoa) {
        this.maMonAn = maMonAn;
        this.tenMonAn = tenMonAn;
        this.donGia = donGia;
        this.soLanDat = soLanDat;
        this.anhMinhHoa = anhMinhHoa;
    }

    public Integer getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(Integer maMonAn) {
        this.maMonAn = maMonAn;
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) {
        this.tenMonAn = tenMonAn;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public int getSoLanDat() {
        return soLanDat;
    }

    public void setSoLanDat(int soLanDat) {
        this.soLanDat = soLanDat;
    }

    public String getAnhMinhHoa() {
        return anhMinhHoa;
    }

    public void setAnhMinhHoa(String anhMinhHoa) {
        this.anhMinhHoa = anhMinhHoa;
    }

    public CuaHang getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(CuaHang maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maMonAn != null ? maMonAn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MonAnCuaHang)) {
            return false;
        }
        MonAnCuaHang other = (MonAnCuaHang) object;
        if ((this.maMonAn == null && other.maMonAn != null) || (this.maMonAn != null && !this.maMonAn.equals(other.maMonAn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tungthai.foodapp2.entity.MonAnCuaHang[ maMonAn=" + maMonAn + " ]";
    }
    
}
