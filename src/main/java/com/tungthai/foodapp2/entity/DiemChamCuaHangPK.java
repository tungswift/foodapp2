/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tungthai
 */
@Embeddable
public class DiemChamCuaHangPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "MaCuaHang")
    private int maCuaHang;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaTaiKhoan")
    private int maTaiKhoan;

    public DiemChamCuaHangPK() {
    }

    public DiemChamCuaHangPK(int maCuaHang, int maTaiKhoan) {
        this.maCuaHang = maCuaHang;
        this.maTaiKhoan = maTaiKhoan;
    }

    public int getMaCuaHang() {
        return maCuaHang;
    }

    public void setMaCuaHang(int maCuaHang) {
        this.maCuaHang = maCuaHang;
    }

    public int getMaTaiKhoan() {
        return maTaiKhoan;
    }

    public void setMaTaiKhoan(int maTaiKhoan) {
        this.maTaiKhoan = maTaiKhoan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) maCuaHang;
        hash += (int) maTaiKhoan;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiemChamCuaHangPK)) {
            return false;
        }
        DiemChamCuaHangPK other = (DiemChamCuaHangPK) object;
        if (this.maCuaHang != other.maCuaHang) {
            return false;
        }
        if (this.maTaiKhoan != other.maTaiKhoan) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tungthai.foodapp2.entity.DiemChamCuaHangPK[ maCuaHang=" + maCuaHang + ", maTaiKhoan=" + maTaiKhoan + " ]";
    }
    
}
