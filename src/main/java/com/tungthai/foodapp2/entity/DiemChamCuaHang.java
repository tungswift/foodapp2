/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tungthai.foodapp2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tungthai
 */
@Entity
@Table(name = "DiemChamCuaHang")
public class DiemChamCuaHang implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DiemChamCuaHangPK diemChamCuaHangPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DiemCham")
    private int diemCham;
    @JoinColumn(name = "MaTaiKhoan", referencedColumnName = "MaTaiKhoan", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private TaiKhoan taiKhoan;

    public DiemChamCuaHang() {
    }

    public DiemChamCuaHang(DiemChamCuaHangPK diemChamCuaHangPK) {
        this.diemChamCuaHangPK = diemChamCuaHangPK;
    }

    public DiemChamCuaHang(DiemChamCuaHangPK diemChamCuaHangPK, int diemCham) {
        this.diemChamCuaHangPK = diemChamCuaHangPK;
        this.diemCham = diemCham;
    }

    public DiemChamCuaHang(int maCuaHang, int maTaiKhoan) {
        this.diemChamCuaHangPK = new DiemChamCuaHangPK(maCuaHang, maTaiKhoan);
    }

    public DiemChamCuaHangPK getDiemChamCuaHangPK() {
        return diemChamCuaHangPK;
    }

    public void setDiemChamCuaHangPK(DiemChamCuaHangPK diemChamCuaHangPK) {
        this.diemChamCuaHangPK = diemChamCuaHangPK;
    }

    public int getDiemCham() {
        return diemCham;
    }

    public void setDiemCham(int diemCham) {
        this.diemCham = diemCham;
    }

    public TaiKhoan getTaiKhoan() {
        return taiKhoan;
    }

    public void setTaiKhoan(TaiKhoan taiKhoan) {
        this.taiKhoan = taiKhoan;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (diemChamCuaHangPK != null ? diemChamCuaHangPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DiemChamCuaHang)) {
            return false;
        }
        DiemChamCuaHang other = (DiemChamCuaHang) object;
        if ((this.diemChamCuaHangPK == null && other.diemChamCuaHangPK != null) || (this.diemChamCuaHangPK != null && !this.diemChamCuaHangPK.equals(other.diemChamCuaHangPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tungthai.foodapp2.entity.DiemChamCuaHang[ diemChamCuaHangPK=" + diemChamCuaHangPK + " ]";
    }
    
}
