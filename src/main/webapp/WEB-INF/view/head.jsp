<%-- 
    Document   : head
    Created on : Nov 6, 2017, 7:52:24 PM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<meta name="google-signin-client_id" content="303075933611-8npb4oo9pcfbdrgo11havdt9ghi239l9.apps.googleusercontent.com">

<spring:url value="/resources/css/style.css" var="style" />
<spring:url value="/resources/css/app.css" var="app" />
<spring:url value="/resources/css/phantrang.css" var="phantrang" />
<spring:url value="/resources/css/bootstrap.min.css" var="bootstrap" />
<spring:url value="/resources/js/dautrangchu.js?v=1" var="dautrangchu" />
<spring:url value="/resources/js/slideShow.js" var="slideShow" />
<spring:url value="/resources/js/tab.js" var="tab" />
<spring:url value="/resources/js/getLocation.js" var="getLocation" />
<spring:url value="/resources/js/popper.min.js" var="popper" />
<spring:url value="/resources/js/jquery-3.2.1.min.js" var="jquery" />
<spring:url value="/resources/js/bootstrap.min.js" var="bootstrapjs" />
<spring:url value="/resources/js/notify.min.js" var="notify" />
<spring:url value="/resources/js/jquery.rateyo.min.js" var="rateyo" />

<!--Cach 1-->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" />

<!--Cach 2-->
<link rel="stylesheet" href="${style}" />
<link rel="stylesheet" href="${app}"/>
<link rel="stylesheet" href="${phantrang}"/>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

<!--Script chung phần chọn cửa hàng-->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="${dautrangchu}"></script>
<script src="https://use.fontawesome.com/14625c452e.js"></script>
<style>
    .address {
        height: 56px;
    }
</style>


