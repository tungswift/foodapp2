<%-- 
    Document   : footer
    Created on : Nov 6, 2017, 8:14:28 PM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="footer">
    <div class="container">
        <div class="row about">
            <div class="col-md-6">
                <h3>Thông Tin</h3>
                <ul>
                    <li>Địa Chỉ: Học viện Công nghệ Bưu chính Viễn thông</li>
                    <li>Điện Thoại: 0988.192.713</li>
                    <li>Email: tungthai13@gmail.com</li>
                </ul>
            </div>

        </div>
    </div>
</div>

<!--Javascript-->
<script src="${popper}"></script>
<script src="${jquery}"></script>
<script src="${bootstrapjs}"></script>
<script src="${notify}"></script>
<script src="${rateyo}"></script>

