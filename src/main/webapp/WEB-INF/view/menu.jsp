<%-- 
    Document   : menu
    Created on : Nov 6, 2017, 7:52:31 PM
    Author     : tungthai
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="menu">
    <nav class="navbar navbar-expand-md fixed-top navbar-light bg-faded bg-menu">
        <a class="navbar-brand" href="home">
            <spring:url var="image" value="/resources/image/"/>
            <img src="${image}logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a id="nav0" class="nav-link" href="home">Trang chủ <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a id='nav3' class="nav-link" href="CuaHang?loaiCuaHang=3">Đồ Ăn </a>
                </li>
                <li class="nav-item">
                    <a id='nav1' class="nav-link" href="CuaHang?loaiCuaHang=1"/>Đồ Uống </a>
                </li>
                <li class="nav-item">
                    <a id='nav2' class="nav-link" href="CuaHang?loaiCuaHang=2">Bánh </a>
                </li> 
            </ul>
            <div id="anhDaiDien"></div>
            <div id="status"></div>
            &nbsp;&nbsp;
            <div class="login btn-succes" id="login">
                <a href="DangNhap" id="dangnhap">Đăng nhập</a>
                <a onclick="fbLogoutUser();" id="dangxuat" hidden="true">Đăng xuất</a>
                <div class="g-signin2" data-onsuccess="onSignIn" style="display: none;"></div>
            </div>

            <script>
                if (checkCookie("username") !== "") {
                    document.getElementById("dangnhap").hidden = true;
                    document.getElementById("login").hidden = true;
//                    document.getElementById("dangxuat").hidden = false;
//                    document.getElementById('status').innerHTML =
//                            '<img style="border-radius: 50%;" src="' + localStorage.getItem("anhDaiDien") + '" width="38" height="38">' + '\u00A0\u00A0' + getCookie("username")
//                            + '\u00A0';
                    document.getElementById('anhDaiDien').innerHTML = '<img style="border-radius: 50%;" src="' + localStorage.getItem("anhDaiDien") + '" width="38" height="38"> \u00A0\u00A0';
                    document.getElementById('status').innerHTML =
                            '   <div class="dropdown">  ' +
                            '       <button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"> ' +
                            '\u00A0\u00A0' + getCookie("username") + '\u00A0' +
                            '       <span class="caret"></span></button>  ' +
                            '       <ul class="dropdown-menu">  ' +
                            '         <li><a href="#" onclick="logout();">Đăng xuất</a></li>  ' +
                            '         <li><a href="DanhSachDonHang?maTaiKhoan=' + localStorage.getItem("maTaiKhoan") + '">Đơn hàng</a></li>  ' +
                            '       </ul>  ' +
                            '    </div>  ';
                }

                document.getElementById("dangnhap").style.cursor = "pointer";
            </script>

        </div>
    </nav>
</div>
