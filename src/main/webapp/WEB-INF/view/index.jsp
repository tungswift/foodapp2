<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.tungthai.foodapp2.entity.CuaHang"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/resources/image/" var="image" />
<!DOCTYPE html>
<html>
    <head>
        <title>Index</title>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <div id="all">

            <!--menu-->
            <%@include file="menu.jsp" %>

            <!--slide-->
            <div id="slide" onLoad="runShow()">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 item-slide">
                            <div class="section-demo">
                                <button onclick="plusDivs(-1)">❮ Prev</button>
                                <button onclick="plusDivs(1)">Next ❯</button>
                            </div>
                            <%
                                List<CuaHang> cuaHangMoi = (List<CuaHang>) request.getAttribute("cuaHangMoi");
                                int dem = 1;
                                for (CuaHang ch : cuaHangMoi) {
                            %>
                            <div class="demo" onclick="currentDiv(<%=dem%>)" style="width: 400px; height: 110px">
                                <div class="item-img">
                                    <img src="${image}<%=ch.getLogo()%>" alt="" width="50" height="50">
                                </div>
                                <div class="para" style="padding-left: 62px">
                                    <p><strong><%=ch.getTenCuaHang()%></strong></p>
                                    <p><%=ch.getDiaChi()%></p>
                                </div>
                            </div>
                            <% dem++;
                                } %>
                        </div>
                        <div class="col-md-7 img-slide">
                            <div onmouseover="stopShow()" onmouseout="runShow()">
                                <%
                                    for (CuaHang ch : cuaHangMoi) {
                                %>
                                <a href="#">
                                    <img class="mySlides" src="${image}<%=ch.getLogo()%>" height="416" width="100%">
                                </a>
                                <%
                                    }%>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--content-->
            <div id="content">
                <div class="container">

                    <!--Tab-->
                    <div class="row select">
                        <div class="col-md-4">
                            <div class="tab">
                                <ul id="tabItem">
                                    <li class="London active-slide" id='London1' onclick="openCity('London')">
                                        <span class="w3-bar-item w3-button London" >Cửa hàng</span>
                                    </li>
                                    <li class="Tokyo" id='Tokyo1' onclick="openCity('Tokyo')" hidden="true">
                                        <span class="w3-bar-item w3-button Tokyo" > Đã Mua</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <form class="form-inline" style="height: 49px;" action="TimKiem">
                                <label class="sr-only" for="inlineFormInput">Name</label>
                                <input type="text" name="timKiem" class="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Tìm kiếm cửa hàng">
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                            </form>
                        </div>
                    </div>
                    <!--End tab-->

                    <!--Tab 1 content-->
                    <div class="row product w3-container w3-display-container city tabLondon" id="London">

                        <%
                            String trangHienTai = String.valueOf(request.getAttribute("trangHienTai"));
                            List<CuaHang> list = (List<CuaHang>) request.getAttribute("danhSachCuaHang");
                            int tongSoTrang = Integer.parseInt(String.valueOf(request.getAttribute("tongSoTrang")));
                            for (CuaHang cuaHang : list) {
                                int tongDiem = cuaHang.getTongDiem();
                                int soLuotCham = cuaHang.getSoLuotCham();
                                float diem;
                                if (soLuotCham == 0) {
                                    diem = 0;
                                } else {
                                    diem = (float) tongDiem / (float) soLuotCham;
                                }

                        %>

                        <!--Store Item-->
                        <div class="col-md-4 default">
                            <div class="item-product">
                                <div class="image">
                                    <a href="#"><img src="${image}<%=cuaHang.getLogo()%>" alt=""></a>

                                </div>
                                <div class="title">
                                    <a href="#">
                                        <div class="star">
                                            <span><%=new DecimalFormat("#0.0").format(diem)%></span>
                                        </div>
                                        <div class="address">
                                            <strong><%=cuaHang.getTenCuaHang()%></strong><br>
                                            <span>
                                                <%=cuaHang.getDiaChi()%>
                                            </span>
                                        </div>

                                    </a>
                                </div>
                                <div class="cach"></div>
                                <div class="order">
                                    <form action="DatMon" method="post" onsubmit="return kiemTraDangNhap();">
                                        <input type="hidden" name="latHome" class="latHome" value="" />
                                        <input type="hidden" name="lngHome" class="lngHome" value="" />
                                        <input type="hidden" name="maCuaHang" value="<%=cuaHang.getMaCuaHang()%>" />
                                        <input type="hidden" name="maTaiKhoan" class="maTaiKhoan" value="" />

                                        <input type="submit" class="storeButton btn btn-submit" value="Đặt món"/>
                                    </form>
                                </div>
                                <!--End button-->

                            </div>
                        </div>
                        <!--End Store Item-->

                        <% }%>
                        <div class="container" style="text-align: center">
                            <div class="pagination text-center" style="margin:0 auto;">
                                <input type="hidden" id="trangHienTai" value="<%=trangHienTai%>"/>
                                <%
                                    int prev;
                                    int next;
                                    if ((Integer.parseInt(trangHienTai) - 1) <= 0) {
                                        prev = 1;
                                    } else {
                                        prev = Integer.parseInt(trangHienTai) - 1;
                                    }

                                    if ((Integer.parseInt(trangHienTai) + 1) > tongSoTrang) {
                                        next = tongSoTrang;
                                    } else {
                                        next = Integer.parseInt(trangHienTai) + 1;
                                    }
                                    if (String.valueOf(request.getAttribute("timKiem")).equals("false")) {
                                %>
                                <a href="ChuyenTrang?trangHienTai=<%=prev%>">&laquo;</a>
                                <%
                                    for (int i = 1; i <= tongSoTrang; i++) {
                                %>
                                <a id="trang<%=i%>" href="ChuyenTrang?trangHienTai=<%=i%>"> <%=i%> </a>
                                <% }%>
                                <a href="ChuyenTrang?trangHienTai=<%=next%>">&raquo;</a>

                                <script>
                                    activePhanTrang();
                                </script>
                                <% } else {%>

                                <a href="TimKiem?trangHienTai=<%=prev%>&timKiem=${timKiem}">&laquo;</a>
                                <%
                                    for (int i = 1; i <= tongSoTrang; i++) {
                                %>
                                <a id="trang<%=i%>" href="TimKiem?trangHienTai=<%=i%>&timKiem=${timKiem}"> <%=i%> </a>
                                <% }%>
                                <a href="TimKiem?trangHienTai=<%=next%>&timKiem=${timKiem}">&raquo;</a>

                                <script>
                                    activePhanTrang();
                                </script>
                                <% }%>
                            </div>
                        </div>

                    </div>

                    <!--End tab 1 content-->

                    <!--Tab 3 content-->
                    <div class="row product w3-container w3-display-container city" id="Tokyo" style="display:none">

                    </div>
                    <!--End tab 3 content-->



                </div>
            </div>


            <!--List user-->


            <!--Footer-->
            <%@include file="footer.jsp" %>

        </div>
    </body>

    <%@include file="script_trang_chu.jsp" %>
</html>
