<%-- 
    Document   : scriptTrangChu
    Created on : Dec 1, 2017, 6:42:44 PM
    Author     : tungthai13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!--Script slide-->
<script type="text/javascript" src="${slideShow}"></script>

<!--Script tab-->
<script type="text/javascript" src="${tab}"></script>

<!--Lấy vị trí hiện tại-->
<script type="text/javascript" src="${getLocation}"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>

<script>
    var listMaTaiKhoan = document.getElementsByClassName("maTaiKhoan");
    for (var i = 0; i < listMaTaiKhoan.length; i++) {
        listMaTaiKhoan[i].value = parseInt(localStorage.getItem("maTaiKhoan"));
    }
</script>