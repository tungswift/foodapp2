<%-- 
    Document   : listuser
    Created on : Nov 6, 2017, 8:14:39 PM
    Author     : tungthai
--%>

<%@page import="dao.TaiKhoanDAO"%>
<%@page import="entity.TaiKhoan"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div id="list-user">
    <div class="container">
        <h4>Thành viên mới</h4>
        <div class="row">
            <% 
                List<TaiKhoan> danhSachTaiKhoan = new TaiKhoanDAO().danhSachTaiKhoan();
                for(TaiKhoan t : danhSachTaiKhoan){
                    
            %>
            <div class="col-md-1">
                <div class="item-user">
                    <img src="<%=t.getAnhDaiDien()%>" alt="" width="70" height="70">
                </div>
            </div>
            <% } %>

        </div>
    </div>

</div>
