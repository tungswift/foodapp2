<%-- 
    Document   : dat_mon
    Created on : Jan 20, 2018, 2:05:00 PM
    Author     : tungthai
--%>

<%@page import="com.tungthai.foodapp2.entity.*"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.Locale"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:url value="/resources/image/" var="image" />
<!DOCTYPE html>
<html>
    
    <head>
        <%@include file="head.jsp" %>
        <title>JSP Page</title>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.2.1.min.js"></script>
        <style>

            #map {
                width: 100%;
                height: 450px;
                padding-bottom: 5px
            }

            .fb-comments {
                margin: auto;
                width: 50%;
            }
        </style>
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=2002167313362798';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <%
            request.setCharacterEncoding("UTF-8"); //Để cho truyền parameter lên dưới dạng utf-8
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
            DecimalFormat format = (DecimalFormat) nf; //Format định dạng tiền

            String maTaiKhoan = String.valueOf(request.getAttribute("maTaiKhoan"));
            CuaHang ch = (CuaHang) request.getAttribute("cuaHang");
            int maCuaHang = ch.getMaCuaHang();
            String tenCuaHang = ch.getTenCuaHang();
            String diaChiCuaHang = ch.getDiaChi();
            float diem;
            if (ch.getSoLuotCham() != 0) {
                diem = ch.getTongDiem() / ch.getSoLuotCham();
            } else {
                diem = 0;
            }

            int diemCham = Integer.parseInt(String.valueOf(request.getAttribute("diemChamCuaHang")));
        %>
        <input type="hidden" id="tenCH" value="<%=tenCuaHang%>"/>
        <input type="hidden" id="dcCH" value="<%=diaChiCuaHang%>"/>
        <input type="hidden" id="diemCham" value="<%=diemCham%>"/>
        <input type="hidden" id="maTaiKhoan" value="<%=maTaiKhoan%>"/>
        

        <div id="all">
            <!--menu-->
            <%@include file="menu.jsp" %>

            <!--content-->
            <div id="dat-mon">
                <div class="detail-order">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 img">
                                <div class="">
                                    <img src="${image}${cuaHang.getLogo()}" alt="">
                                </div>
                            </div>
                            <div class="col-md-4">

                                <h4 style="padding-top: 20px">${cuaHang.getTenCuaHang()}</h4>
                                <p class="address">${cuaHang.getDiaChi()}</p>
                                <p>Điểm: <span id="diem"><%=diem%></span></p>

                                <div class="time">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <span style="color: green">${cuaHang.getGioMoCua()}:00 - ${cuaHang.getGioDongCua()}:00</span>
                                </div>
                                <!--hien thong bao khoang cach tu javascript-->
                                <div id="khoangCach">
                                    <p>Phí vận chuyển: 5000 đ/km</p>
                                </div>

                                <!--hiện form chấm điểm-->
                                <div id="rateYo" style="padding-left: 0px;"></div>
                                <input type="hidden" name="diemCham" id="diemCham" value="" />

                                <br>

                            </div>
                            <div class="col-md-4 price">
                                <form action="" method="POST" onsubmit="return kiemTraGioHangRong();">
                                    <!--Mã cửa hàng-->
                                    <input type="hidden" name="maCuaHang" id="maCuaHang" value="<%=maCuaHang%>"/>
                                    
                                    <table class="table">
                                        <thead>
                                            <tr>                      
                                                <th><button type="button" class="btn btn-danger" id="reset">Xóa tất cả</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr id="gioHang">

                                                <td>Tổng </td>
                                                <td id="money"> 0 đ </td>
                                            </tr>
                                            <tr>
                                                <td>      
                                                    <input type="hidden" id="json" name="json" value=""/>
                                                    <input type="hidden" id="tenCuaHang" name="tenCuaHang" value=""/>
                                                    <input type="hidden" id="diaChiCuaHang" name="diaChiCuaHang" value=""/>
                                                    <input type="submit" class="pull-right btn btn-success" value="Đặt Mua"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>   
                            </div>
                        </div>

                        <div class="row">
                            <input type="hidden" value="${lat}" id="lat">
                            <input type="hidden" value="${lng}" id="lng">
                            <input type="hidden" value="${lngCuaHang}" id="lngCuaHang">
                            <input type="hidden" value="${latCuaHang}" id="latCuaHang">

                            <!--Google map-->   
                            <div id="map"></div>
                            <!--End google map-->
                        </div>
                    </div>
                </div>

                <div class="lien-quan">
                    <div class="container">
                        <div class="row">
                            <h3>Thực đơn</h3>
                        </div>
                        <div class="row">
                            <%
                                List<MonAnCuaHang> list = (List<MonAnCuaHang>) request.getAttribute("danhSachMonAn");

                                for (MonAnCuaHang thucDon : list) {

                            %>
                            <div class="col-md-6 box">
                                <div class="left pull-left">
                                    <div class="img">
                                        <img src="${image}<%=thucDon.getAnhMinhHoa()%>" alt="">
                                    </div>
                                    <div class="item">
                                        <strong><%=thucDon.getTenMonAn()%></strong><br>
                                        <span>Đã được đặt <%=thucDon.getSoLanDat()%> lần</span>
                                    </div>
                                </div>
                                <div class="right pull-right">

                                    <!--<form action="themmon" method="post">-->
                                    <strong><%=format.format(thucDon.getDonGia())%> VNĐ</strong>
                                    <input type="hidden" id="tenMon<%=thucDon.getMaMonAn()%>" value="<%=thucDon.getTenMonAn()%>" />
                                    <input type="hidden" id="donGia<%=thucDon.getMaMonAn()%>" value="<%=thucDon.getDonGia()%>" />
                                    <i class="fa fa-plus themMon" aria-hidden="true" id="them<%=thucDon.getMaMonAn()%>"></i>
                                    <i class="fa fa-minus botMon" aria-hidden="true" id="bot<%=thucDon.getMaMonAn()%>"></i>
                                    <!--</form>-->

                                </div>
                            </div>   
                            <% }%>
                            <!--End mon an-->
                        </div>

                        <!--Comment facebook row-->   
                        <div class="row">
                            <div class="fb-comments" data-numposts="5"></div>
                            <script>
                                var maCuaHang = document.getElementById("maCuaHang").value;
                                $(".fb-comments").attr("data-href", window.location.href + "?maCuaHang=" + maCuaHang);
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <!--List user-->

            <!--Footer-->
            <%@include file="footer.jsp" %>
        </div>
    </body>
</html>

<!--script chấm điểm (Sử dụng ajax)-->
<script src="${pageContext.request.contextPath}/resources/js/chamdiem.js"></script>

<!--script xử lý giỏ hàng-->
<script src="${pageContext.request.contextPath}/resources/js/xulygiohang.js?v=2"></script>

<!--Script xử lý google map-->
<script src="${pageContext.request.contextPath}/resources/js/xulygooglemap.js"></script>

<!--Google map-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA84UAqytUxGlER7GBT2E723Wjo3Pwlafg&callback=initMap" async defer></script>

