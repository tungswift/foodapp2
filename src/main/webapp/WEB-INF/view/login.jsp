<%-- 
    Document   : login
    Created on : Nov 19, 2017, 6:00:39 PM
    Author     : tungthai13
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Đăng Nhập</title>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <script>

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '2002167313362798',
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.8' // use graph api version 2.8
                });

            };

            // Load the SDK asynchronously
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            // Here we run a very simple test of the Graph API after login is
            // successful.  See statusChangeCallback() for when this call is made.
            function testAPI() {
                //console.log('Welcome!  Fetching your information.... ');
                FB.api('/me', function (response) {
                    setCookie("username", response.name, 1);
                    var facebookId = response.id;
                    var ten = response.name;
                    var anhDaiDien = "http://graph.facebook.com/" + facebookId + "/picture?type=square";
                    setCookie("anhDaiDien", anhDaiDien, 1);
                    $.ajax({
                        url: 'KiemTraDangNhap',
                        data: {
                            facebookId: facebookId,
                            anhDaiDien: anhDaiDien,
                            tenTaiKhoan: ten
                        },
                        type: 'get',
                        cache: false,
                        success: function (data) {
                            localStorage.setItem("maTaiKhoan", data);
                            localStorage.setItem("anhDaiDien", anhDaiDien);
                        },
                        error: function () {
                            alert('error');
                        }
                    });
                    document.location.href = 'home';

                });
            }

            function setCookie(cname, cvalue, exdays) {
                var d = new Date();
                d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                var expires = "expires=" + d.toUTCString();
                document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            }
        </script>

        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <table class="table">
                    <tr>
                        <th><h2>Đăng nhập</h2></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <!--Nut login facebook-->
                            <div onlogin="testAPI();" class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
                        </td>
                        <td>
                            <!--Nut google do dai 236 = facebook-->
                            <div class="g-signin2" data-width="236" data-onsuccess="onSignIn"></div> 
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4"></div>
        </div>



    </body>

    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=2002167313362798';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function onSignIn(googleUser) {
            var profile = googleUser.getBasicProfile();

            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            setCookie("username", profile.getName(), 1);
            var email = profile.getEmail();
            var ten = profile.getName();
            var anhDaiDien = profile.getImageUrl();
            setCookie("anhDaiDien", anhDaiDien, 1);
            $.ajax({
                url: 'KiemTraDangNhap',
                data: {
                    email: email,
                    tenTaiKhoan: ten,
                    anhDaiDien: anhDaiDien
                },
                type: 'get',
                cache: false,
                success: function (data) {
                    localStorage.setItem("maTaiKhoan", data);
                    localStorage.setItem("anhDaiDien", anhDaiDien);
                },
                error: function () {
                    alert('error');
                }
            });
            var idToken = profile.id_token;
            googleUser.disconnect();
            document.location.href = 'home';
        }

        function signOut() {
            gapi.auth2.getAuthInstance().signOut();
        }
    </script>

    <!--Javascript-->
    <script src="${popper}"></script>
    <script src="${jquery}"></script>
    <script src="${bootstrapjs}"></script>
    <script src="${notify}"></script>

</html>
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>

