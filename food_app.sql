-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 03, 2018 at 11:37 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `food_app`
--
CREATE DATABASE IF NOT EXISTS `food_app` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `food_app`;

-- --------------------------------------------------------

--
-- Table structure for table `CuaHang`
--

DROP TABLE IF EXISTS `CuaHang`;
CREATE TABLE `CuaHang` (
  `MaCuaHang` int(11) NOT NULL,
  `TenCuaHang` varchar(300) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `DiaChi` varchar(300) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `LoaiCuaHang` varchar(300) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `Logo` varchar(300) NOT NULL DEFAULT 'CuaHang.jpg',
  `GioMoCua` int(11) NOT NULL,
  `GioDongCua` int(11) NOT NULL,
  `TongDiem` int(11) NOT NULL DEFAULT '0',
  `SoLuotCham` int(11) NOT NULL DEFAULT '0',
  `ToaDoLat` double DEFAULT NULL,
  `ToaDoLng` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `CuaHang`
--

INSERT INTO `CuaHang` (`MaCuaHang`, `TenCuaHang`, `DiaChi`, `LoaiCuaHang`, `Logo`, `GioMoCua`, `GioDongCua`, `TongDiem`, `SoLuotCham`, `ToaDoLat`, `ToaDoLng`) VALUES
(1, 'Chả cá Anh Vũ', '116-K1 Giảng Võ, Hà Nội', 'Đồ ăn', 'chacaanhvu.jpeg', 10, 22, 0, 0, 21.027081, 105.824167),
(2, 'Le Castella Viet Nam', '7 Huỳnh Thúc Kháng, Quận Đống Đa, Hà Nội', 'Bánh', 'LeCastella.jpg', 9, 22, 0, 0, 21.017049, 105.813588);

-- --------------------------------------------------------

--
-- Table structure for table `DiemChamCuaHang`
--

DROP TABLE IF EXISTS `DiemChamCuaHang`;
CREATE TABLE `DiemChamCuaHang` (
  `MaCuaHang` int(11) NOT NULL,
  `MaTaiKhoan` int(11) NOT NULL,
  `DiemCham` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `DonHang`
--

DROP TABLE IF EXISTS `DonHang`;
CREATE TABLE `DonHang` (
  `MaDonHang` int(11) NOT NULL,
  `MaTaiKhoan` int(11) NOT NULL,
  `NgayDat` int(11) NOT NULL,
  `TongTien` int(11) NOT NULL,
  `TrangThaiDonHang` varchar(200) NOT NULL,
  `MaCuaHang` int(11) NOT NULL,
  `NgayGiaoHang` varchar(20) NOT NULL,
  `GioGiaoHang` varchar(20) NOT NULL,
  `SoDienThoai` varchar(20) NOT NULL,
  `DiaChiGiaoHang` varchar(300) NOT NULL,
  `GhiChu` varchar(300) NOT NULL,
  `PhiGiaoHang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MonAnCuaHang`
--

DROP TABLE IF EXISTS `MonAnCuaHang`;
CREATE TABLE `MonAnCuaHang` (
  `MaMonAn` int(11) NOT NULL,
  `TenMonAn` varchar(300) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `MaCuaHang` int(11) NOT NULL,
  `DonGia` int(11) NOT NULL,
  `SoLanDat` int(11) NOT NULL DEFAULT '0',
  `AnhMinhHoa` varchar(300) NOT NULL DEFAULT 'MonAn.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MonAnCuaHang`
--

INSERT INTO `MonAnCuaHang` (`MaMonAn`, `TenMonAn`, `MaCuaHang`, `DonGia`, `SoLanDat`, `AnhMinhHoa`) VALUES
(1, 'Mon thu 1', 1, 111111, 0, 'BanhMiThapCam.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `MonAnDonHang`
--

DROP TABLE IF EXISTS `MonAnDonHang`;
CREATE TABLE `MonAnDonHang` (
  `MaDonHang` int(11) NOT NULL,
  `MaMonAn` int(11) NOT NULL,
  `SoLuong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TaiKhoan`
--

DROP TABLE IF EXISTS `TaiKhoan`;
CREATE TABLE `TaiKhoan` (
  `MaTaiKhoan` int(11) NOT NULL,
  `TenTaiKhoan` varchar(300) NOT NULL,
  `Email` varchar(300) DEFAULT NULL,
  `AnhDaiDien` text NOT NULL,
  `FacebookID` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TaiKhoan`
--

INSERT INTO `TaiKhoan` (`MaTaiKhoan`, `TenTaiKhoan`, `Email`, `AnhDaiDien`, `FacebookID`) VALUES
(1, 'Tùng Thái', 'tungthai13@gmail.com', 'https://lh4.googleusercontent.com/-sj3SRygAVZs/AAAAAAAAAAI/AAAAAAAAAAA/AA6ZPT5fYC_wIEG0y4CWW9S48sOjbqp6-w/s96-c/photo.jpg', 'null'),
(2, 'Tung Swift', 'boy.ps3@gmail.com', 'https://lh6.googleusercontent.com/-1xfhbzElrMM/AAAAAAAAAAI/AAAAAAAAAAA/AA6ZPT7k9Facf4gYwa-6DLgX_Rw_7mw4wA/s96-c/photo.jpg', 'null');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `CuaHang`
--
ALTER TABLE `CuaHang`
  ADD PRIMARY KEY (`MaCuaHang`);

--
-- Indexes for table `DiemChamCuaHang`
--
ALTER TABLE `DiemChamCuaHang`
  ADD PRIMARY KEY (`MaCuaHang`,`MaTaiKhoan`),
  ADD KEY `FK_DiemChamTaiKhoan` (`MaTaiKhoan`);

--
-- Indexes for table `DonHang`
--
ALTER TABLE `DonHang`
  ADD PRIMARY KEY (`MaDonHang`),
  ADD KEY `FK_DonHang_TaiKhoan` (`MaTaiKhoan`),
  ADD KEY `FK_DonHang_CuaHang` (`MaCuaHang`);

--
-- Indexes for table `MonAnCuaHang`
--
ALTER TABLE `MonAnCuaHang`
  ADD PRIMARY KEY (`MaMonAn`),
  ADD KEY `FK_CuaHangMonAn` (`MaCuaHang`);

--
-- Indexes for table `MonAnDonHang`
--
ALTER TABLE `MonAnDonHang`
  ADD PRIMARY KEY (`MaDonHang`,`MaMonAn`),
  ADD KEY `FK_DonHang_MonAn` (`MaMonAn`);

--
-- Indexes for table `TaiKhoan`
--
ALTER TABLE `TaiKhoan`
  ADD PRIMARY KEY (`MaTaiKhoan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `CuaHang`
--
ALTER TABLE `CuaHang`
  MODIFY `MaCuaHang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `DonHang`
--
ALTER TABLE `DonHang`
  MODIFY `MaDonHang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `MonAnCuaHang`
--
ALTER TABLE `MonAnCuaHang`
  MODIFY `MaMonAn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `TaiKhoan`
--
ALTER TABLE `TaiKhoan`
  MODIFY `MaTaiKhoan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `DiemChamCuaHang`
--
ALTER TABLE `DiemChamCuaHang`
  ADD CONSTRAINT `FK_DiemChamTaiKhoan` FOREIGN KEY (`MaTaiKhoan`) REFERENCES `TaiKhoan` (`MaTaiKhoan`);

--
-- Constraints for table `DonHang`
--
ALTER TABLE `DonHang`
  ADD CONSTRAINT `FK_DonHang_CuaHang` FOREIGN KEY (`MaCuaHang`) REFERENCES `CuaHang` (`MaCuaHang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_DonHang_TaiKhoan` FOREIGN KEY (`MaTaiKhoan`) REFERENCES `TaiKhoan` (`MaTaiKhoan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `MonAnCuaHang`
--
ALTER TABLE `MonAnCuaHang`
  ADD CONSTRAINT `FK_CuaHangMonAn` FOREIGN KEY (`MaCuaHang`) REFERENCES `CuaHang` (`MaCuaHang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `MonAnDonHang`
--
ALTER TABLE `MonAnDonHang`
  ADD CONSTRAINT `FK_DonHang_MonAn` FOREIGN KEY (`MaMonAn`) REFERENCES `MonAnCuaHang` (`MaMonAn`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
